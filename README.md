Name: Indu Mohanan
mail id: imohanan@usc.edu

Answers

1. Part1 Spam: 	 Precision: 0.949468085106383
		 Recall: 0.9834710743801653
		 F-Score: 0.96617050067659
	Ham:	 Precision: 0.993920972644377
		 Recall:  0.981
		 F-Score: 0.9874182184197282

	Pos: 	 Precision: 0.9746738296239448
		 Recall: 0.9143268538516919
		 F-Score: 0.9435364041604755
	Neg:	 Precision: 0.9289976133651552
		 Recall:  0.9792452830188679
		 F-Score: 0.9534598897734232

2. Part2
	SVM
	Spam: 	 Precision:0.9855491329479769 
		 Recall: 0.9393939393939394
		 F-Score: 0.9619181946403385 
	Ham:	 Precision: 0.9783677482792527
		 Recall: 0.995
		 F-Score: 0.9866137828458106

	Pos: 	 Precision: 0.9289232934553132
		 Recall: 0.9503239740820735
		 F-Score: 0.9395017793594306
	Neg:	 Precision: 0.9557124518613607 
		 Recall:  0.9364779874213837
		 F-Score: 0.945997458703939

	MegaM
	Spam: 	 Precision: 0.9102564102564102
		 Recall: 0.7823691460055097
		 F-Score: 0.8414814814814815
	Ham:	 Precision: 0.9248334919124643
		 Recall: 0.972
		 F-Score: 0.9478303266699171

	Pos: 	 Precision: 0.6546153846153846
		 Recall: 0.6126709863210943
		 F-Score: 0.6329490516920788
	Neg:	 Precision: 0.6795711733174509
		 Recall: 0.7176100628930817
		 F-Score: 0.6980728051391862

3. On using only 10% of the files. 
	Part1
	The F-score increased very slightly for SPAM and HAm 
	The F-score slightly increased for POS and NEG. This might be due to overlapping of a large portion of the Dev and Training set.
	Part2
	SVM: F-score improved for POS and NEG.
	MegaM: F-score increased substancially for Pos and Neg. This could be due to overlap of dev and training set.

Commands to run files

python3 nbCreate.py

python3 nblearn.py spam_training.txt spam.nb
python3 nblearn.py sentiment_training.txt sentiment.nb

python3 nbclassify.py spam.nb spam_test.txt >spam.out
python3 nbclassify.py sentiment.nb sentiment_test.txt >sentiment.out


