#
#	Name: Indu Mohanan
#	email: imohanan@usc.edu
#

import math
import sys
import json

def createSpamModelFile():
	args = sys.argv
	spamArguments={}
	hamArguments={} 

	spamWordCount={}
	spamTotalWords=0
	probSpamWord={}
	hamWordCount={}
	hamTotalWords=0
	probHamWord={}

	vocab={}

	spamCount=0
	hamCount=0
	vocabSize=0
	totalCount=0
	finalArguments={}
	
	with open(args[1], 'r', errors='ignore') as ipFile:
		lines=ipFile.readlines()
		for line in lines:
			words=line.split(' ') 
			if words[0]=='SPAM':				
				totalCount=totalCount+1
				spamCount=spamCount+1
				for word in words:
					spamTotalWords+=1
					if word in spamWordCount:
						spamWordCount[word]= spamWordCount[word]+1
					else:
						spamWordCount[word]=1
						if word not in vocab:
							vocab[word]=1
							vocabSize+=1
			elif  words[0]=='HAM':
				totalCount=totalCount+1
				hamCount=hamCount+1
				for word in words:
					hamTotalWords+=1
					if word in hamWordCount:
						hamWordCount[word]+=1
					else:
						hamWordCount[word]=1
						if word not in vocab:
							vocab[word]=1
							vocabSize+=1
		# to find probability of eachWord with Smoothening
		for word in vocab:
			if word in spamWordCount:
				probSpamWord[word] = math.log((spamWordCount[word]+1)/(spamTotalWords+vocabSize))
			else:
				probSpamWord[word] = math.log(1/(spamTotalWords+vocabSize))
				
			if word in hamWordCount:
				probHamWord[word] = math.log((hamWordCount[word]+1)/(hamTotalWords+vocabSize))
			else:
				probHamWord[word] = math.log(1/(hamTotalWords+vocabSize))
		# find arguments
		probSpam= math.log(spamCount/totalCount)
		probHam=math.log(hamCount/totalCount)
		

		# save to arguments
		spamArguments['spamCount']=spamCount
		spamArguments['probSpam']=probSpam
		#spamArguments['spamWordCount']=spamWordCount
		spamArguments['spamTotalWords']=spamTotalWords
		spamArguments['probSpamWord']=probSpamWord

		hamArguments['hamCount']=hamCount		
		hamArguments['probHam'] = probHam	
		#hamArguments['hamWordCount']=hamWordCount
		hamArguments['hamTotalWords'] = hamTotalWords
		hamArguments['probHamWord'] = probHamWord

		finalArguments['totalMailCount']=totalCount		
		finalArguments['spamArguments'] = spamArguments	
		finalArguments['hamArguments'] = hamArguments	
		#finalArguments['vocab']=vocab
		finalArguments['vocabSize'] = vocabSize
		return finalArguments
		#print arguments to test
		#print("finalArguments: ")
		#print(finalArguments)

def createSentimentModelFile():
	args = sys.argv
	posArguments={}
	negArguments={}

	posWordCount={}
	posTotalWords=0
	probPosWord={}
	negWordCount={}
	negTotalWords=0
	probNegWord={}

	vocab={}

	posCount=0
	negCount=0
	vocabSize=0
	totalCount=0
	finalArguments={}

	with open(args[1], 'r', errors='ignore') as ipFile:
		lines = ipFile.readlines()
		for line in lines:
			words=line.split(' ') 
			if len(words)!=0 and words[0]=='POS':
				totalCount=totalCount+1
				posCount=posCount+1
				for word in words:
					posTotalWords+=1
					if word !='POS' and word in posWordCount:
						posWordCount[word]= posWordCount[word]+1
					elif word !='POS':
						posWordCount[word]=1
						if word not in vocab:
							vocab[word]=1
							vocabSize+=1

			elif len(words)!=0 and words[0]=='NEG':
				totalCount=totalCount+1
				negCount=negCount+1
				for word in words:
					negTotalWords+=1
					if word !='NEG' and word in negWordCount:
						negWordCount[word]+=1
					elif word !='NEG':
						negWordCount[word]=1
						if word not in vocab:
							vocab[word]=1
							vocabSize+=1
		# to find probability of eachWord with Smoothening
		for word in vocab:
			if word in posWordCount:
				probPosWord[word] = math.log((posWordCount[word]+1)/(posTotalWords+vocabSize))
			else:
				probPosWord[word] = math.log(1/(posTotalWords+vocabSize))
				
			if word in negWordCount:
				probNegWord[word] = math.log((negWordCount[word]+1)/(negTotalWords+vocabSize))
			else:
				probNegWord[word] = math.log(1/(negTotalWords+vocabSize))
		# find arguments
		probPos= math.log(posCount/totalCount)
		probNeg=math.log(negCount/totalCount)
		

		# save to arguments
		posArguments['posCount']=posCount
		posArguments['probPos']=probPos
		#posArguments['posWordCount']=posWordCount
		posArguments['posTotalWords']=posTotalWords
		posArguments['probPosWord']=probPosWord

		negArguments['negCount']=negCount		
		negArguments['probNeg'] = probNeg	
		#negArguments['negWordCount']=negWordCount
		negArguments['negTotalWords'] = negTotalWords
		negArguments['probNegWord'] = probNegWord

		finalArguments['totalCount']=totalCount		
		finalArguments['posArguments'] = posArguments	
		finalArguments['negArguments'] = negArguments	
		#finalArguments['vocab']=vocab
		finalArguments['vocabSize'] = vocabSize
		return finalArguments

def writeSpamToFile(finalArguments):
	jsonSpamData={}
	jsonSpamData['finalArguments']=finalArguments
	with open(args[2],"w+",encoding="utf-8") as out_file:
		json.dump(jsonSpamData,out_file, indent=4, ensure_ascii=False)	

def writeSentimentToFile(finalArguments):
	jsonSentimentData={}
	jsonSentimentData['finalArguments']=finalArguments
	with open(args[2],"w+",encoding="utf-8") as out_file:
		json.dump(jsonSentimentData,out_file, indent=4, ensure_ascii=False)	

if __name__=='__main__':
	args = sys.argv
	if args[1]=='spam_training.txt':
		finalArguments = createSpamModelFile()
		writeSpamToFile(finalArguments)

	if args[1]=='sentiment_training.txt':
		finalArguments = createSentimentModelFile()
		writeSentimentToFile(finalArguments)

