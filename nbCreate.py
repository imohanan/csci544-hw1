import sys
import os

def createSpamTrainingFile():
	folderPath='/home/indu/Desktop/544/hw1/training-set/SPAM_training/'
	#folderPath='/home/indu/Desktop/544/hw1/test/'
	opFile=open('spam_training.txt','w')
	opFile.close()
	fileNames=os.listdir(folderPath)
	fileNames.sort()
	for fileName in fileNames:
		filenameFinal=fileName.split('.')
		with open('spam_training.txt','a') as opFile:
			opFile.write(filenameFinal[0]+" ")

		ipFile=open(folderPath+fileName,"r",encoding = "utf-8",errors="ignore")
		with open('spam_training.txt','a') as opFile:
			for line in ipFile:
				for char in line:
					if char!='\n':
						opFile.write('%s'%char)
					else:
	       					opFile.write(" ")
		with open('spam_training.txt','a') as opFile:
			opFile.write("\n")

def createSentimentTrainingFile():
	count=0
	folderPath='/home/indu/Desktop/544/hw1/sentiment/SENTIMENT_training/'		
	opFile=open('sentiment_training.txt','w+')
	opFile.close()
	fileNames=os.listdir(folderPath)
	fileNames.sort()
	for fileName in fileNames:
		filenameFinal=fileName.split('.')
		with open('sentiment_training.txt','a') as opFile:
			if count==0:
					#opFile.write(fileName+' ')
					opFile.write(filenameFinal[0]+" ")
					count+=1
			else: opFile.write("\n"+filenameFinal[0]+" ")
		with open(folderPath+fileName,"r",encoding = "utf-8",errors="ignore") as ipFile:
			with open('sentiment_training.txt','a') as opFile:
				for line in ipFile:
					for char in line:
						if char!='\n':
							opFile.write('%s'%char)
						else:
		       					opFile.write(" ")
		with open('sentiment_training.txt','a') as opFile:
			opFile.write("\n")

def createSpamTestingFile():
	count=0
	count1=0
	folderPath='/home/indu/Desktop/544/hw1/SPAM_test/'
	opFile=open('spam_test.txt','w')
	opFile.close()
	#opFile=open('spam_testOutput.txt','w')
	#opFile.close()
	fileNames=os.listdir(folderPath)
	fileNames.sort()
	for fileName in fileNames:
		#filenameFinal=fileName.split('.')
		#with open('spam_test.txt','a') as opFile:
		#	if count==0: count+=1			
		#	else: opFile.write("\n")
		#with open('spam_testOutput.txt','a') as opFile:
		#	if count1==0: 
		#		count1+=1	
		#		opFile.write(filenameFinal[0])			
		#	else: opFile.write("\n"+filenameFinal[0])	
		with open(folderPath+fileName, 'r', errors='ignore') as ipFile:
			with open('spam_test.txt','a') as opFile:
				for line in ipFile:
					for char in line:
						if char!='\n':
							opFile.write('%s'%char)
						else:
		       					opFile.write(" ")
		with open('spam_test.txt','a') as opFile:
			opFile.write("\n")
		

def createSentimentTestingFile():
	count=0
	count1=0
	folderPath='/home/indu/Desktop/544/hw1/SENTIMENT_test/'
	opFile=open('sentiment_test.txt','w')
	opFile.close()
	#opFile=open('sentiment_testOutput.txt','w')
	#opFile.close()
	fileNames=os.listdir(folderPath)
	fileNames.sort()
	for fileName in fileNames:
		#filenameFinal=fileName.split('.')
		#with open('sentiment_test.txt','a') as opFile:
		#	if count==0: count+=1			
		#	else: opFile.write("\n")
		#with open('sentiment_testOutput.txt','a') as opFile:
		#	if count1==0: 
		#		count1+=1	
		#		opFile.write(filenameFinal[0])			
		#	else: opFile.write("\n"+filenameFinal[0])
		with open(folderPath+fileName, 'r', errors='ignore') as ipFile:
			txt = ipFile.readlines()
			for i in range(len(txt)):
				wordList=txt[i].replace('\n',' ')
				with open('sentiment_test.txt','a') as opFile:
					for word in wordList:
						opFile.write(word)
				for line in ipFile:
					for char in line:
						if char!='\n':
							opFile.write('%s'%char)
						else:
		       					opFile.write(" ")
		with open('sentiment_test.txt','a') as opFile:
			opFile.write("\n")

def compareResults():
	total=0
	incorrect=0
	correct=0
	outputLines=[]
	testLines=[]
	with open('spam.out','r') as generatedFile:
		generatedLines = generatedFile.readlines()		
	with open('spam_testOutput.txt','r') as outputFile:
		outputLines = outputFile.readlines()
	for i in range(len(generatedLines)):	
		total+=1
		generatedLines[i]=generatedLines[i].replace('\n','')
		outputLines[i]=outputLines[i].replace('\n','')
		if generatedLines[i]!=outputLines[i]:	
			incorrect+=1
		else:	correct+=1
	print(correct*100/total)
 
if __name__=='__main__':
	createSpamTrainingFile()
	createSentimentTrainingFile()
	createSpamTestingFile()
	createSentimentTestingFile()
	#compareResults()
