#
#	Name: Indu Mohanan
#	email: imohanan@usc.edu
#

import sys
import json
import math

def classifySpamData():
	jsonData={}
	with open(args[1], 'r', errors='ignore') as jsonFile:
		jsonData=json.load(jsonFile)
	with open(args[2], 'r', errors='ignore') as testFile:
		lines = testFile.readlines()
		for line in lines:
			# find and compare probability of spam and ham
			words=line.split(' ')
			probMsgSpam=0
			probMsgHam=0
			for word in words:
				word=word.replace('\n',' ')
				if word in jsonData['finalArguments']['spamArguments']['probSpamWord']:
					probSpamWord = jsonData['finalArguments']['spamArguments']['probSpamWord'][word]
				else:
					probSpamWord = math.log(1/( jsonData['finalArguments']['spamArguments']['spamTotalWords'] + jsonData['finalArguments']['vocabSize']+1))
				probMsgSpam+=probSpamWord

				if word in jsonData['finalArguments']['hamArguments']['probHamWord']:
					probHamWord = jsonData['finalArguments']['hamArguments']['probHamWord'][word]
				else:
					probHamWord = math.log(1/( jsonData['finalArguments']['hamArguments']['hamTotalWords'] + jsonData['finalArguments']['vocabSize']+1))
				probMsgHam+=probHamWord
	
			probMsgSpam+=jsonData['finalArguments']['spamArguments']['probSpam']
			probMsgHam+=jsonData['finalArguments']['hamArguments']['probHam']			
			if probMsgSpam> probMsgHam:
				print('SPAM')
			else:
				print('HAM')

def classifySentimentData():
	jsonData={}
	with open(args[1], 'r', errors='ignore') as jsonFile:
		jsonData=json.load(jsonFile)
	with open(args[2], 'r', errors='ignore') as testFile:
		lines = testFile.readlines()
		for line in lines:
			# find and compare probability of spam and ham
			words=line.split(' ')
			probMsgPos=0
			probMsgNeg=0
			for word in words:
				word=word.replace('\n',' ')
				if word in jsonData['finalArguments']['posArguments']['probPosWord']:
					probPosWord = jsonData['finalArguments']['posArguments']['probPosWord'][word]
				else:
					probPosWord = math.log(1/( jsonData['finalArguments']['posArguments']['posTotalWords'] + jsonData['finalArguments']['vocabSize']+1))
				probMsgPos+=probPosWord
				if word in jsonData['finalArguments']['negArguments']['probNegWord']:
					probNegWord = jsonData['finalArguments']['negArguments']['probNegWord'][word]
				else:
					probNegWord = math.log(1/( jsonData['finalArguments']['negArguments']['negTotalWords'] + jsonData['finalArguments']['vocabSize']+1))
				probMsgNeg+=probNegWord
	
			probMsgPos+=jsonData['finalArguments']['posArguments']['probPos']
			probMsgNeg+=jsonData['finalArguments']['negArguments']['probNeg']			

			if probMsgPos> probMsgNeg:
				print('POS')
			else:
				print('NEG')

				


if __name__=='__main__':
	args = sys.argv
	if args[1]=='spam.nb':
		classifySpamData()
	if args[1]=='sentiment.nb':
		classifySentimentData()
